
#include <stdint.h>
#include <stddef.h>

#include "sdr_definitions.h"
#include "sensors_templates.h"


/*
 * Sensor reading callbacks
 *
 * These callbacks are assigned to each sensor when the sensors are created.
 * They are defined at the end of this file
 *
 * Sensors are create in the create_board_specific_sensors() hook below.
 *
 *
 */
sensor_reading_status_t sensor_reading_fpga_temp(sensor_reading_t* sensor_reading);
sensor_reading_status_t sensor_reading_air_temp(sensor_reading_t* sensor_reading);
sensor_reading_status_t sensor_reading_vcc_out(sensor_reading_t* sensor_reading);



/*
 * Hook for creating board specific sensors
 *
 * This function is called during the OpenIPMC initialization and is dedicated
 * for creating the board-specific sensors.
 */
void create_board_specific_sensors(void)
{

  // Dummy sensor for FPGA temperature.
  uint8_t const threshold_list_fpga_temp[6] =
  {
    0,    // Lower Non Recoverable NOT USED
    0,    // Lower Critical        NOT USED
    0,    // Lower Non Critical    NOT USED
    65,   // Upper Non Critical    65°C
    75,   // Upper Critical        75°C
    100   // Upper Non Recoverable 100°C
  };

  analog_sensor_1_init_t const sensor_init_struct_fpga_temp =
  {
    .sensor_type             = TEMPERATURE,
    .base_unit_type          = DEGREES_C,
    .m                       = 1,            // y = 1*x + 0  (temperature in °C is identical to it raw value)
    .b                       = 0,
    .b_exp                   = 0,
    .r_exp                   = 0,
    .threshold_mask_read     = 1,
    .threshold_mask_set      = UPPER_NON_CRITICAL | UPPER_CRITICAL | UPPER_NON_RECOVERABLE,
    .threshold_list          = threshold_list_fpga_temp,
    .id_string               = "FPGA TEMP",
    .get_sensor_reading_func = &sensor_reading_fpga_temp,
    .sensor_action_req       = NULL
  };

  create_generic_analog_sensor_1(&sensor_init_struct_fpga_temp);


  // Dummy sensor for Air temperature.
  uint8_t const threshold_list_air_temp[6] =
  {
    0,    // Lower Non Recoverable  NOT USED
    0,    // Lower Critical         NOT USED
    0,    // Lower Non Critical     NOT USED
    100,  // Upper Non Critical     30°C  (see conversion below)
    120,  // Upper Critical         40°C
    0     // Upper Non Recoverable  NOT USED
  };

  analog_sensor_1_init_t const sensor_init_struct_air_temp =
  {
    .sensor_type             = TEMPERATURE,
    .base_unit_type          = DEGREES_C,
    .m                       = 5,            // y = (0.5*x - 20) = (5*x - 200)*0.1
    .b                       = -200,
    .b_exp                   = 0,
    .r_exp                   = -1,
    .threshold_mask_read     = 1,
    .threshold_mask_set      = UPPER_NON_CRITICAL | UPPER_CRITICAL,
    .threshold_list          = threshold_list_air_temp,
    .id_string               = "AIR TEMP",
    .get_sensor_reading_func = &sensor_reading_air_temp,
    .sensor_action_req       = NULL
  };

  create_generic_analog_sensor_1(&sensor_init_struct_air_temp);


  // Dummy sensor for Voltage.
  uint8_t const threshold_list_voltage[6] =
  {
    0,    // Lower Non Recoverable  NOT USED
    0,    // Lower Critical         NOT USED
    0,    // Lower Non Critical     NOT USED
    0,    // Upper Non Critical     NOT USED
    0  ,  // Upper Critical         NOT USED
    0     // Upper Non Recoverable  NOT USED
  };

  analog_sensor_1_init_t const sensor_init_struct_voltage =
  {
    .sensor_type             = VOLTAGE,
    .base_unit_type          = VOLTS,
    .m                       = 1,            // y = 0.1*x = (1*x + 0)*0.1
    .b                       = 0,
    .b_exp                   = 0,
    .r_exp                   = -1,
    .threshold_mask_read     = 0,
    .threshold_mask_set      = 0,
    .threshold_list          = threshold_list_voltage,
    .id_string               = "12V_RAIL",
    .get_sensor_reading_func = &sensor_reading_vcc_out,
    .sensor_action_req       = NULL
  };

  create_generic_analog_sensor_1(&sensor_init_struct_voltage);
}


/*
 * Sensor Reading functions
 */
sensor_reading_status_t sensor_reading_fpga_temp(sensor_reading_t* sensor_reading)
{

	// This sensor uses y = 1*x + 0 for conversion

	uint8_t raw_temp = 53; // 53°C

	// Fill the raw temp field
	sensor_reading->raw_value = raw_temp;

	// Fill the threshold flag field
	sensor_reading->present_state = 0;
	if(raw_temp > 65)
		sensor_reading->present_state |= UPPER_NON_CRITICAL;
	if(raw_temp > 75)
		sensor_reading->present_state |= UPPER_CRITICAL;
	if(raw_temp > 100)
		sensor_reading->present_state |= UPPER_NON_RECOVERABLE;

	return SENSOR_READING_OK;
}


sensor_reading_status_t sensor_reading_air_temp(sensor_reading_t* sensor_reading)
{

	// This sensor uses y = (0.5*x - 20) for conversion

	uint8_t raw_temp = 97; // 28.5°C

	// Fill the raw temp field
	sensor_reading->raw_value = raw_temp;

	// Fill the threshold flag field
	sensor_reading->present_state = 0;
	if(raw_temp > 100) // 30°C
		sensor_reading->present_state |= UPPER_NON_CRITICAL;
	if(raw_temp > 120) // 40°C
		sensor_reading->present_state |= UPPER_CRITICAL;

	return SENSOR_READING_OK;
}

sensor_reading_status_t sensor_reading_vcc_out(sensor_reading_t* sensor_reading)
{

	// This sensor uses y = 0.1*x for conversion
	sensor_reading->raw_value = 124; // 12.4V

	sensor_reading->present_state = 0; // No thresholds supported by this sensor

	return SENSOR_READING_OK;
}
