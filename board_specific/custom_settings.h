#ifndef CUSTOM_SETTINGS
#define CUSTOM_SETTINGS


#include "cmsis_os.h"

/*
 * The Custom Startup Task is the first board-specific task launched by
 * OpenIPMC-FW. User can define its size and priority here.
 */
#define CUSTOM_STARTUP_TASK_STACK_SIZE 128  // In 4byte words
#define CUSTOM_STARTUP_TASK_PRIORITY osPriorityNormal

/*
 * TerminalProcessTask is responsible to process the terminal commands by calling
 * the command callbacks. User-defined commands may require more memory
 */
#define TERMINAL_PROCESS_TASK_STACK_SIZE 512  // In 4byte words

/*
 * FruStateMachineTask is responsible to change the activation state of the
 * ATCA Payload, which implies in calling the board_specific_activation_policy()
 * callback, defined in the board-specific code.
 *
 * Users may need to increase this stack to accommodate custom activation process.
 */
#define OPENIPMC_FRU_STATE_MACHINE_TASK_STACK_SIZE 512  // In 4byte words

/*
 * IpmiIncomingRequestmanagerTask solves the IPMI requests from Shelf Manager,
 * including the sensor reading, which depends on user-defined callbacks.
 *
 * Users may need to increase this stack to accommodate custom sensor reading
 * process.
 */
#define OPENIPMC_INCOMING_REQUESTS_TASK_STACK_SIZE 512  // In 4byte words

/*
 * Memory for terminal commands is statically allocated.
 * Users may need to increase this parameter according their needs.
 */
#define TERMINAL_MAX_NUMBER_OF_COMMANDS 28


// Set if DHCP is ON or OFF by default (0 = OFF; 1 = ON)
#define DHCP_STARTUP_STATE 1


/*
 * Set if the CLI (IPMC terminal console) is available on the DIMM UART0
 * (0 = NOT AVAILABLE; 1 = AVAILABLE)
 */
#define CLI_AVAILABLE_ON_UART 1

#endif
