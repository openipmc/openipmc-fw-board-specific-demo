
.PHONY := all batch init hpm full_image clean

.DEFAULT_GOAL := all

all: hpm full_image

batch: init all

# Guarantee updated submodules
init:
	@echo Updating Submodules
	@git submodule update --init --recursive

# Generate the .bin and .hpm into openipmc-fw/CM7 and 
# openipmc-fw/CM4 directories.
# For convenience, these files are copied into the project root
hpm: hpm_firmware hpm_bootloader 

hpm_firmware:
	@cd openipmc-fw                                     && \
	 $(MAKE) hpm                                        && \
	 cp CM4/openipmc-fw_CM4.bin ../openipmc-fw_CM4.bin  && \
	 cp CM4/upgrade_CM4.hpm     ../upgrade_CM4.hpm      && \
	 cp CM7/openipmc-fw_CM7.bin ../openipmc-fw_CM7.bin  && \
	 cp CM7/upgrade_CM7.hpm     ../upgrade_CM7.hpm

hpm_bootloader:
	@cd openipmc-fw-bootloader                                     && \
	 $(MAKE) hpm                                                   && \
	 cp CM7/openipmc-fw-bootloader_CM7.bin ../openipmc-fw_BL7.bin  && \
	 cp CM7/upgrade_BL7.hpm                ../upgrade_BL7.hpm

full_image: hpm
	@cp openipmc-fw_CM4.bin openipmc-fw-full-image-builder/ && \
	 cp openipmc-fw_CM7.bin openipmc-fw-full-image-builder/ && \
	 cp openipmc-fw_BL7.bin openipmc-fw-full-image-builder/ && \
	 cd openipmc-fw-full-image-builder && \
	 $(MAKE) full_image && \
	 cp openipmc-fw_full.bin ../

# NOTE: this target is not executed in the defaults
tar: hpm full_image
	@tar -cvf openipmc-fw_binaries.tar.gz \
	 upgrade_CM4.hpm \
	 openipmc-fw_CM4.bin \
	 upgrade_CM4.hpm \
	 openipmc-fw_CM7.bin \
	 upgrade_CM7.hpm \
	 openipmc-fw_BL7.bin \
	 upgrade_BL7.hpm \
	 openipmc-fw_full.bin

clean:
	@cd openipmc-fw && $(MAKE) clean
	@cd openipmc-fw-bootloader && $(MAKE) clean
	@cd openipmc-fw-full-image-builder && $(MAKE) clean
	@rm -f openipmc-fw-full-image-builder/openipmc-fw_CM7.bin
	@rm -f openipmc-fw-full-image-builder/openipmc-fw_CM4.bin
	@rm -f openipmc-fw-full-image-builder/openipmc-fw_BL7.bin
	@rm -f openipmc-fw_BL7.bin
	@rm -f openipmc-fw_CM4.bin
	@rm -f openipmc-fw_CM7.bin
	@rm -f upgrade_BL7.hpm
	@rm -f upgrade_CM4.hpm
	@rm -f upgrade_CM7.hpm
	@rm -f openipmc-fw_full.bin
	@rm -f openipmc-fw_binaries.tar.gz
